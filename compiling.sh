
javac -cp src/libs_custom/jakarta.inject-api-1.0.jar:src/libs_custom/jakarta.servlet-api-4.0.4.jar\
      -d src/main/webapp/WEB-INF/classes/ src/main/java/org/jboss/as/quickstarts/helloworld/*

cd src/main/

# com pots veure els sources no queden dins del war
jar cvf wildfly-helloworld.war -C webapp .

mv wildfly-helloworld.war ../../../../wildfly-26.1.3.Final/standalone/deployments/
